<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AbcRainWidget</name>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="80"/>
        <source>Press space bar to start</source>
        <translation>Leertaste startet das Spiel</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="105"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="107"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="110"/>
        <source>E&amp;xit Game</source>
        <translation>Spiel &amp;beenden</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="361"/>
        <source>Number of points:</source>
        <translation>Erreichte Punktzahl:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="426"/>
        <source>Points:</source>
        <translation>Punkte:</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="425"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../games/abcrainwidget.cpp" line="437"/>
        <source>Press space bar to proceed</source>
        <translation>Leertaste setzt das Spiel fort</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="60"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <location filename="../sql/chartablesql.cpp" line="90"/>
        <source>Characters</source>
        <translation>Schriftzeichen</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="91"/>
        <source>Target Errors</source>
        <translation>Soll-Fehler</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="92"/>
        <source>Actual Errors</source>
        <translation>Ist-Fehler</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="93"/>
        <source>Frequency</source>
        <translation>Vorkommen</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="94"/>
        <source>Error Rate</source>
        <translation>Fehlerquote</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="97"/>
        <source>This column shows all of the
characters typed</source>
        <translation>Diese Spalte zeigt alle bislang
eingegebenen Schriftzeichen</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="101"/>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>Ein &quot;Soll-Fehler&quot; entsteht, wenn ein anderes
Schriftzeichen eingegeben wurde als das hier
vorgegebene</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="104"/>
        <source>Character was mistyped</source>
        <translation>Ein &quot;Ist-Fehler&quot; entsteht, wenn das Schriftzeichen
trotz anderer Vorgabe eingegeben wurde</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="106"/>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>Diese Spalte gibt an, wie oft das Schriftzeichen
ingesamt diktiert wurde</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="110"/>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>Die Fehlerquote zeigt, welche Schriftzeichen
Ihnen am meisten Probleme bereiten.
Die Fehlerquote errechnet sich aus dem
Wert &quot;Soll-Fehler&quot; und dem Wert
&quot;Vorkommen&quot;.</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="134"/>
        <source>Reset characters</source>
        <translation>Schriftzeichen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../sql/chartablesql.cpp" line="207"/>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation>Die aufgezeichneten Fehlerquoten beeinflussen die Intelligenzfunktion und damit auch die Auswahl der zu diktierenden Texte. Wenn die Fehlerquote eines bestimmten Zeichens uebermaessig hoch ist, kann es unter Umstaenden sinnvoll sein, die Liste zurueckzusetzen.

Es werden nun alle aufgezeichneten Schriftzeichen geloescht.

Wollen Sie den Vorgang wirklich fortsetzen?
</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <location filename="../widget/errormessage.cpp" line="82"/>
        <source>The process will be aborted.</source>
        <translation>Der Vorgang wird abgebrochen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="85"/>
        <source>The update will be aborted.</source>
        <translation>Das Update wird abgebrochen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="88"/>
        <source>The program will be aborted.</source>
        <translation>Die Anwendung wird beendet.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="99"/>
        <source>Cannot load the program logo.</source>
        <translation>Das Programmlogo konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="102"/>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Ein Tastatur-Bitmap konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="105"/>
        <source>Cannot load the timer background.</source>
        <translation>Der Lauftext-Hintergund konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="108"/>
        <source>Cannot load the status bar background.</source>
        <translation>Der Statusleisten-Hintergund konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="113"/>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Die Datenbank %1 im Programmverzeichnis konnte nicht gefunden werden.
Sie muss existieren, um die Software starten zu können.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="118"/>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation>Im angegebenen Verzeichnis konnte keine Datenbank gefunden werden. Es wird nun versucht, eine neue, leere Datenbank in diesem Verzeichnis anzulegen.

Den Verzeichnispfad zur Datenbank können Sie in den Einstellungen verändern.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="124"/>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Die Benutzer-Datenbank konnte nicht in Ihrem HOME-Verzeichnis angelegt werden. Eventell fehlen die Schreibrechte.
Es wird nun versucht, die Original-Datenbank im Programmverzeichnis zu verwenden.

Den Verzeichnispfad zur Datenbank können Sie anschließend in den Einstellungen verändern.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="131"/>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Die Benutzer-Datenbank konnte nicht im angegebenen Verzeichnis angelegt werden. Eventell fehlt das Verzeichnis oder es sind keine Schreibrechte vorhanden.
Es wird nun versucht, eine Datenbank in Ihrem HOME-Verzeichnis anzulegen.

Den Verzeichnispfad zur Datenbank können Sie anschließend in den Einstellungen verändern.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="138"/>
        <location filename="../widget/errormessage.cpp" line="142"/>
        <source>Connection to the database failed.</source>
        <translation>Die Verbindung zur Datenbank ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="145"/>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>Die Benutzertabelle mit den Lektionendaten kann nicht
geleert werden.
SQL-Statement fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="149"/>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>Die Benutzertabelle mit den Fehlerdaten kann nicht
geleert werden.
SQL-Statement fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="153"/>
        <source>No lessons exist.</source>
        <translation>Keine Lektionen vorhanden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="156"/>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>Es wurde keine Lektion selektiert.
Bitte wählen Sie eine Lektion aus.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="159"/>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Lektion konnte nicht erstellt werden.
SQL-Statement fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="163"/>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>Die Lektion konnte nicht aktualisiert werden, weil kein
Zugriff auf die Datenbank möglich ist.

Falls dieses Problem erst auftrat, nachem der Schreibtrainer
zuvor einige Zeit anstandslos lief, ist voraussichtlich die
Datenbank beschädigt worden (z.B. durch einen Absturz des
Computers).
Um zu überprüfen, ob die Datenbank beschädigt wurde, können
Sie die Datenbank-Datei testweise einmal umbenennen und die
Software dann neu starten (es sollte dann auomatisch eine
neue, leere Datenbank angelegt werden). Den Pfad zur Datenbank
&quot;%1&quot; können Sie den Grundeinstellungen entnehmen.

Wenn dieses Problem gleich nach dem ersten Start der Software
auftrat, fehlen voraussichtlich die Schreibrechte auf die
Datenbank-Datei. Bitte überprüfen Sie diese.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="178"/>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>Die Benutzertabelle mit den Fehlerdaten konnte nicht
aktualisiert werden, weil kein Zugriff auf die Datenbank
möglich ist.

Falls dieses Problem erst auftrat, nachem der Schreibtrainer
zuvor einige Zeit anstandslos lief, ist voraussichtlich die
Datenbank beschädigt worden (z.B. durch einen Absturz des
Computers).
Um zu überprüfen, ob die Datenbank beschädigt wurde, können
Sie die Datenbank-Datei testweise einmal umbenennen und die
Software dann neu starten (es sollte dann auomatisch eine
neue, leere Datenbank angelegt werden). Den Pfad zur Datenbank
&quot;%1&quot; können Sie den Grundeinstellungen entnehmen.

Wenn dieses Problem gleich nach dem ersten Start der Software
auftrat, fehlen voraussichtlich die Schreibrechte auf die
Datenbank-Datei. Bitte überprüfen Sie diese.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="193"/>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>Die Benutzertabelle mit den Lektionendaten konnte nicht
aktualisiert werden, weil kein Zugriff auf die Datenbank
möglich ist.

Falls dieses Problem erst auftrat, nachem der Schreibtrainer
zuvor einige Zeit anstandslos lief, ist voraussichtlich die
Datenbank beschädigt worden (z.B. durch einen Absturz des
Computers).
Um zu überprüfen, ob die Datenbank beschädigt wurde, können
Sie die Datenbank-Datei testweise einmal umbenennen und die
Software dann neu starten (es sollte dann auomatisch eine
neue, leere Datenbank angelegt werden). Den Pfad zur Datenbank
&quot;%1&quot; können Sie den Grundeinstellungen entnehmen.

Wenn dieses Problem gleich nach dem ersten Start der Software
auftrat, fehlen voraussichtlich die Schreibrechte auf die
Datenbank-Datei. Bitte überprüfen Sie diese.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="207"/>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Die Lektion konnte nicht gespeichert werden.
SQL-Statement fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="210"/>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation>Die Lektion konnte nicht angefordert werden.
SQL-Statement fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="213"/>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation>Die Lektion konnte nicht analysiert werden.
SQL-Statement fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="216"/>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>Die Datei konnte nicht importiert werden.
Bitte überprüfen Sie, ob es sich um eine lesbare Textdatei handelt.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="221"/>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>Die Datei konnte nicht importiert werden, weil sie leer ist.
Bitte überprüfen Sie, ob es sich um eine lesbare Textdatei mit
Inhalt handelt.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="226"/>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>Die Datei konnte leider nicht importiert werden.
Überprüfen Sie bitte die Schreibweise der Internetadresse,
es muss sich um eine lesbare Textdatei und um eine gültige
URL handeln.
Überprüfen Sie bitte außerdem Ihre Internetverbindung.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="231"/>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>Die Datei konnte leider nicht exportiert werden.
Bitte überprüfen Sie, ob es sich um eine beschreibbare Textdatei handelt.
</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="235"/>
        <source>Cannot create temporary file.</source>
        <translation>Temporäre Datei konnte nicht erzeugt werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="238"/>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Das Update konnte nicht durchgeführt werden.
Bitte überprüfen Sie Ihre Internetverbindung und die Proxyeinstellung.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="242"/>
        <source>Cannot read the online update version.</source>
        <translation>Die Online-Versionsinformation konnte nicht gelesen werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="245"/>
        <source>Cannot read the database update version.</source>
        <translation>Die Datenbank-Versionsinformation konnte nicht gelesen werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="248"/>
        <source>Cannot execute the SQL statement.</source>
        <translation>SQL-String konnte nicht verarbeitet werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="251"/>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Keine Tippfehler-Definitonen vorhanden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="254"/>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Temporäre Datei konnte nicht erzeugt werden.
Das Update wird abgebrochen.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="257"/>
        <source>Cannot create analysis table.</source>
        <translation>Analyse-Tabelle kann nicht erstellt werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="260"/>
        <source>Cannot create analysis index.</source>
        <translation>Analyse-Index kann nicht erstellt werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="263"/>
        <source>Cannot fill analysis table with values.</source>
        <translation>Analyse-Tabelle kann nicht mit Inhalten gefüllt werden.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="267"/>
        <source>An error has occured.</source>
        <translation>Ein Fehler ist aufgetreten.</translation>
    </message>
    <message>
        <location filename="../widget/errormessage.cpp" line="271"/>
        <source>
(Error number: %1)
</source>
        <translation>
(Fehlernummer: %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="62"/>
        <source>Report</source>
        <translation>Bericht</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="66"/>
        <source>Overview of Lessons</source>
        <translation>Lektionenübersicht</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="67"/>
        <source>Progress of Lessons</source>
        <translation>Lektionsverlauf</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="68"/>
        <source>Characters</source>
        <translation>Schriftzeichen</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="69"/>
        <source>Fingers</source>
        <translation>Finger</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="70"/>
        <source>Comparison Table</source>
        <translation>Vergleichstabelle</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="89"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="90"/>
        <source>&amp;Close</source>
        <translation>&amp;Fertig</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="172"/>
        <source>Use examples</source>
        <translation>Beispielbewertungen</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="176"/>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation>Beachten Sie, dass langsames Tippen ohne Fehler bessere Bewertungen hervorruft, als schnelles Tippen mit vielen Fehlern!</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="186"/>
        <source>Score</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="191"/>
        <source>For example, this equates to …</source>
        <translation>Entspricht zum Beispiel …</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="196"/>
        <source>Performance</source>
        <translation>Leistung</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="203"/>
        <location filename="../widget/evaluationwidget.cpp" line="219"/>
        <location filename="../widget/evaluationwidget.cpp" line="230"/>
        <location filename="../widget/evaluationwidget.cpp" line="241"/>
        <location filename="../widget/evaluationwidget.cpp" line="257"/>
        <location filename="../widget/evaluationwidget.cpp" line="268"/>
        <location filename="../widget/evaluationwidget.cpp" line="279"/>
        <location filename="../widget/evaluationwidget.cpp" line="295"/>
        <location filename="../widget/evaluationwidget.cpp" line="306"/>
        <location filename="../widget/evaluationwidget.cpp" line="317"/>
        <location filename="../widget/evaluationwidget.cpp" line="333"/>
        <location filename="../widget/evaluationwidget.cpp" line="344"/>
        <location filename="../widget/evaluationwidget.cpp" line="355"/>
        <location filename="../widget/evaluationwidget.cpp" line="371"/>
        <location filename="../widget/evaluationwidget.cpp" line="382"/>
        <location filename="../widget/evaluationwidget.cpp" line="393"/>
        <location filename="../widget/evaluationwidget.cpp" line="409"/>
        <location filename="../widget/evaluationwidget.cpp" line="420"/>
        <source>Points</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="207"/>
        <location filename="../widget/evaluationwidget.cpp" line="223"/>
        <location filename="../widget/evaluationwidget.cpp" line="234"/>
        <location filename="../widget/evaluationwidget.cpp" line="245"/>
        <location filename="../widget/evaluationwidget.cpp" line="261"/>
        <location filename="../widget/evaluationwidget.cpp" line="272"/>
        <location filename="../widget/evaluationwidget.cpp" line="283"/>
        <location filename="../widget/evaluationwidget.cpp" line="299"/>
        <location filename="../widget/evaluationwidget.cpp" line="310"/>
        <location filename="../widget/evaluationwidget.cpp" line="321"/>
        <location filename="../widget/evaluationwidget.cpp" line="337"/>
        <location filename="../widget/evaluationwidget.cpp" line="348"/>
        <location filename="../widget/evaluationwidget.cpp" line="359"/>
        <location filename="../widget/evaluationwidget.cpp" line="375"/>
        <location filename="../widget/evaluationwidget.cpp" line="386"/>
        <location filename="../widget/evaluationwidget.cpp" line="397"/>
        <location filename="../widget/evaluationwidget.cpp" line="413"/>
        <location filename="../widget/evaluationwidget.cpp" line="424"/>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation>%1 A/min und %2 Fehler in %3 Minuten</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="212"/>
        <source>No experience in touch typing</source>
        <translation>Ohne Erfahrung im Zehnfingersystem</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="250"/>
        <source>First steps in touch typing</source>
        <translation>Erste Erfahrungen im Zehnfingersystem</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="288"/>
        <source>Advanced level</source>
        <translation>Fortgeschritten</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="326"/>
        <source>Suitable skills</source>
        <translation>Brauchbare Leistung</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="364"/>
        <source>Very good skills</source>
        <translation>Sehr gute Leistung</translation>
    </message>
    <message>
        <location filename="../widget/evaluationwidget.cpp" line="402"/>
        <source>Perfect skills</source>
        <translation>Hervorragende Leistung</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="244"/>
        <source>Error rates of your fingers</source>
        <translation>Fehlerquoten der Finger</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="251"/>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation>Die Fehlerquoten werden aus den Schriftzeichen und dem aktuell gewählten Tastaturlayout berechnet.</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="261"/>
        <source>Error Rate:</source>
        <translation>Fehlerquote:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="263"/>
        <source>Frequency:</source>
        <translation>Vorkommen:</translation>
    </message>
    <message>
        <location filename="../widget/fingerwidget.cpp" line="265"/>
        <source>Errors:</source>
        <translation>Fehler:</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="43"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="55"/>
        <location filename="../widget/helpbrowser.cpp" line="59"/>
        <source>en</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="85"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="88"/>
        <source>Table of Contents</source>
        <translation>Inhaltsverzeichnis</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="89"/>
        <source>&amp;Close</source>
        <translation>&amp;Fertig</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="92"/>
        <source>&amp;Print page</source>
        <translation>Seite &amp;drucken</translation>
    </message>
    <message>
        <location filename="../widget/helpbrowser.cpp" line="131"/>
        <source>Print page</source>
        <translation>Seite drucken</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="45"/>
        <source>Introduction</source>
        <translation>Einführung</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="76"/>
        <source>Welcome to TIPP10</source>
        <translation>Willkommen zu TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="81"/>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation>TIPP10 ist ein kostenloser 10-Finger-Schreibtrainer für Windows, Mac OS und Linux. Die Software arbeitet intelligent - Schriftzeichen, die häufig falsch getippt werden, werden auch sofort häufiger diktiert. So lässt sich das Zehnfingersystem schnell und effizient erlernen.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="88"/>
        <source>Tips for using the 10 finger system</source>
        <translation>Tipps zur Anwendung des Zehnfingersystems</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="92"/>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation>1. Die Finger nehmen zunächst die Grundstellung ein (sie wird auch zu Beginn jeder Lektion angezeigt). Nach jedem Tippen einer Taste kehren die Finger in die Grundstellung zurück.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="98"/>
        <source>en</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="102"/>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation>2. Nehmen Sie eine aufrechte Haltung ein und vermeiden Sie es in jedem Fall auf die Tastatur zu sehen. Ihr Blick sollte stets auf den Bildschirm gerichtet sein.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="107"/>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation>3. Legen Sie die Oberarme am Körper an und lassen Sie die Schultern hängen. Die Unterarme bilden einen rechten Winkel zu den Oberarmen. Legen Sie die Handgelenke nicht ab und lassen Sie sie nicht durchhängen.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="113"/>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation>4. Bleiben Sie während des Schreibtrainings entspannt.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="116"/>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation>5. Versuchen Sie möglichst fehlerfrei zu tippen. Es ist deutlich ineffizienter schnell zu tippen, wenn Sie dabei viele Fehler machen.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="120"/>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation>6. Einmal mit dem Zehnfingersystem angefangen, sollten Sie es tunlichst vermeiden wieder zu Ihrem &quot;alten System&quot; zurück zu wechseln (auch wenn es mal schnell gehen muss).</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="124"/>
        <source>If you need assistance with using the software use the help function.</source>
        <translation>Bei Fragen rund um die Bedienung der Software rufen Sie bitte die Hilfe auf.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="135"/>
        <source>All rights reserved.</source>
        <translation>Alle Rechte vorbehalten.</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="149"/>
        <source>&amp;Launch TIPP10</source>
        <translation>TIPP10 &amp;starten</translation>
    </message>
    <message>
        <location filename="../widget/illustrationdialog.cpp" line="153"/>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>Dieses &amp;Fenster nicht mehr anzeigen</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <location filename="../widget/lessondialog.cpp" line="124"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="125"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="126"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="134"/>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Name der Lektion (maximal 20 Zeichen):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="136"/>
        <source>Short description (120 characters max.):</source>
        <translation>Kurzbeschreibung (maximal 120 Zeichen):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="137"/>
        <source>Lesson content (at least two lines):</source>
        <translation>Diktat (mindestens zwei Zeilen):</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="168"/>
        <source>Sentence Lesson</source>
        <translation>Satzdiktat</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="170"/>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Die einzelnen Einheiten werden mit
einem Zeilenumbruch am Ende diktiert</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="175"/>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Die einzelnen Einheiten werden mit
Leerzeichen getrennt diktiert,
ein Zeilenumbruch erfolgt automatisch
nach mindestens %1 Schriftzeichen.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="182"/>
        <source>Edit own Lesson</source>
        <translation>Eigene Lektion editieren</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="248"/>
        <source>Please enter the name of the lesson
</source>
        <translation>Bitte geben Sie der Lektion einen Namen
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="254"/>
        <source>Please enter entire lesson content
</source>
        <translation>Bitte geben Sie ein vollständiges Diktat ein
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="282"/>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Bitte geben Sie maximal 400 Zeilen Diktat ein
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="290"/>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>Der Name der Lektion existiert bereits. Bitte geben Sie der Lektion einen anderen Namen.
</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="173"/>
        <source>Word Lesson</source>
        <translation>Wortdiktat</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="139"/>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Erläuterung&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Jede Zeile (Zeilenumbruch am Ende) entspricht einer Einheit für das Diktat. Das Diktat kann auf zwei Arten geführt werden:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Satzdiktat&lt;/b&gt; - die einzelnen Zeilen (Sätze) werden wie hier eingegeben mit einem Zeilenumbruch am Ende diktiert.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Wortdiktat&lt;/b&gt; - die einzelnen Zeilen (Worte) werden mit Leerzeichen getrennt diktiert, ein Zeilenumbruch erfolgt automatisch nach mindestens %1 diktierten Schriftzeichen.</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="149"/>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;Was passiert bei aktivierter Intelligenz?&lt;/b&gt;&lt;br&gt;Bei aktivierter Intelligenz werden die Zeilen nicht der Reihenfolge nach diktiert, sondern abhängig von den Tippfehler-Quoten die aktuell für den Lernerfolg sinnvollste Zeile in das Diktat einbezogen. Die Intelligenz zu aktivieren macht nur bei Lektionen Sinn, die aus sehr vielen Zeilen bestehen.&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="157"/>
        <source>Dictate the text as:</source>
        <translation>Das Diktat soll geführt werden als:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="184"/>
        <source>Name of the Lesson:</source>
        <translation>Name der Lektion:</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="188"/>
        <source>Create own Lesson</source>
        <translation>Eigene Lektion erstellen</translation>
    </message>
    <message>
        <location filename="../widget/lessondialog.cpp" line="276"/>
        <source>Please enter at least two lines of text
</source>
        <translation>Bitte geben Sie mindestens zwei Zeilen Diktat ein
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="39"/>
        <source>Print Lesson</source>
        <translation>Lektion drucken</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="61"/>
        <source>&amp;Print</source>
        <translation>&amp;Drucken</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="62"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../widget/lessonprintdialog.cpp" line="75"/>
        <source>Please enter your name:</source>
        <translation>Bitte geben Sie Ihren Namen ein:</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <location filename="../widget/lessonresult.cpp" line="65"/>
        <source>Print</source>
        <translation>Drucken</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="96"/>
        <source>Entire Lesson</source>
        <translation>Gesamte Lektion</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="191"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="267"/>
        <location filename="../widget/lessonresult.cpp" line="462"/>
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation>
            <numerusform>Sie haben %1 erreicht, bei einer Schreibgeschwindigkeit von %2 A/min und %n Tippfehler.</numerusform>
            <numerusform>Sie haben %1 erreicht, bei einer Schreibgeschwindigkeit von %2 A/min und %n Tippfehlern.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="285"/>
        <location filename="../widget/lessonresult.cpp" line="480"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="296"/>
        <location filename="../widget/lessonresult.cpp" line="343"/>
        <location filename="../widget/lessonresult.cpp" line="492"/>
        <location filename="../widget/lessonresult.cpp" line="538"/>
        <source>Duration: </source>
        <translation>Dauer: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="300"/>
        <location filename="../widget/lessonresult.cpp" line="496"/>
        <source>Typing Errors: </source>
        <translation>Tippfehler: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="304"/>
        <location filename="../widget/lessonresult.cpp" line="500"/>
        <source>Assistance: </source>
        <translation>Hilfestellungen: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="318"/>
        <location filename="../widget/lessonresult.cpp" line="513"/>
        <source>Results</source>
        <translation>Auswertung</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="335"/>
        <location filename="../widget/lessonresult.cpp" line="530"/>
        <source>Lesson: </source>
        <translation>Lektion: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="339"/>
        <location filename="../widget/lessonresult.cpp" line="534"/>
        <source>Time: </source>
        <translation>Zeitpunkt: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="347"/>
        <location filename="../widget/lessonresult.cpp" line="542"/>
        <source>Characters: </source>
        <translation>Zeichen: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="351"/>
        <location filename="../widget/lessonresult.cpp" line="546"/>
        <source>Errors: </source>
        <translation>Fehler: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="355"/>
        <location filename="../widget/lessonresult.cpp" line="550"/>
        <source>Error Rate: </source>
        <translation>Fehlerquote: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="359"/>
        <location filename="../widget/lessonresult.cpp" line="554"/>
        <source>Cpm: </source>
        <translation>A/min: </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="373"/>
        <location filename="../widget/lessonresult.cpp" line="567"/>
        <source>Dictation</source>
        <translation>Diktat</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="456"/>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation>10-Finger-Schreibtrainer TIPP10</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="459"/>
        <source>Report</source>
        <translation>Bericht</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="459"/>
        <source> of %1</source>
        <translation> von %1</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="584"/>
        <source>Print Report</source>
        <translation>Bericht drucken</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="87"/>
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n Minute</numerusform>
            <numerusform>%n Minuten</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="92"/>
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n Zeichen</numerusform>
            <numerusform>%n Zeichen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="103"/>
        <source>Error Correction with Backspace</source>
        <translation>Fehler korrigieren</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="106"/>
        <source>Error Correction without Backspace</source>
        <translation>Diktat blockieren</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="108"/>
        <source>Ignore Errors</source>
        <translation>Tippfehler übergehen</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="116"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="124"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="132"/>
        <source>- Colored Keys</source>
        <translation>- Farbige Tasten</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="138"/>
        <source>- Home Row</source>
        <translation>- Grundstellung</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="144"/>
        <source>- Motion Paths</source>
        <translation>- Tastpfade</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="150"/>
        <source>- Separation Line</source>
        <translation>- Trennlinie</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="156"/>
        <source>- Instructions</source>
        <translation>- Hilfetext</translation>
    </message>
    <message>
        <location filename="../widget/lessonresult.cpp" line="188"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/lessonresult.cpp" line="201"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n Punkt</numerusform>
            <numerusform>%n Punkte</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="84"/>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="88"/>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="92"/>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <location filename="../sql/lessontablesql.cpp" line="104"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n Punkt</numerusform>
            <numerusform>%n Punkte</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="140"/>
        <source>Show: </source>
        <translation>Zeige: </translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="142"/>
        <source>All Lessons</source>
        <translation>Alle Lektionen</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="143"/>
        <source>Training Lessons</source>
        <translation>Übungslektionen</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="144"/>
        <source>Open Lessons</source>
        <translation>Freie Lektionen</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="145"/>
        <source>Own Lessons</source>
        <translation>Eigene Lektionen</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="287"/>
        <source>Lesson</source>
        <translation>Lektion</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="288"/>
        <source>Time</source>
        <translation>Zeitpunkt</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="289"/>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="290"/>
        <source>Characters</source>
        <translation>Zeichen</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="291"/>
        <source>Errors</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="292"/>
        <source>Rate</source>
        <translation>Quote</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="293"/>
        <source>Cpm</source>
        <translation>A/min</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="294"/>
        <source>Score</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="297"/>
        <source>This column shows the names
of completed lessons</source>
        <translation>Diese Spalte zeigt die Namen
der absolvierten Lektionen</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="301"/>
        <source>Start time of the lesson</source>
        <translation>Startzeitpunkt der Lektion</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="303"/>
        <source>Total duration of the lesson</source>
        <translation>Dauer der Lektion insgesamt</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="304"/>
        <source>Number of characters dictated</source>
        <translation>Anzahl der Schriftzeichen, die
ingesamt diktiert wurden</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="307"/>
        <source>Number of typing errors</source>
        <translation>Anzahl der Tippfehler, die in
der Lektion gemacht wurden</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="309"/>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation>Tippfehler in Abhängigkeit von der
Diktatlänge (Tippfehler / Zeichen)
Umso geringer die Fehlerquote, desto besser!</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="314"/>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>&quot;A/min&quot; besagt, wie viele Schriftzeichen (Anschläge)
durchschnittlich pro Minute eingegeben wurden</translation>
    </message>
    <message>
        <location filename="../sql/lessontablesql.cpp" line="318"/>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>Die Bewertung der Leistung errechnet sich wie folgt:
((Zeichen - (20 x Fehler)) / Dauer in Minuten) x 0.4

Beachten Sie, dass langsames Tippen ohne Fehler
eine bessere Bewertung hervorruft, als schnelles
Tippen mit vielen Fehlern!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widget/mainwindow.cpp" line="76"/>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>Es gehen alle Werte der laufenden Lektion verloren!

Wollen Sie die Anwendung wirklich beenden?

</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="97"/>
        <location filename="../widget/mainwindow.cpp" line="115"/>
        <source>&amp;Go</source>
        <translation>&amp;Gehe zu</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="103"/>
        <location filename="../widget/mainwindow.cpp" line="119"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="111"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="130"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="131"/>
        <source>E&amp;xit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="133"/>
        <source>&amp;Results</source>
        <translation>&amp;Lernstatistik</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="134"/>
        <source>&amp;Manual</source>
        <translation>&amp;Bedienungsanleitung</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="136"/>
        <source> on the web</source>
        <translation> im Internet</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="138"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="140"/>
        <source>&amp;About </source>
        <translation>&amp;Über </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="143"/>
        <source>ABC-Game</source>
        <translation>ABC-Spiel</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="196"/>
        <source>Software Version </source>
        <translation>Programmversion </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="197"/>
        <source>Database Version </source>
        <translation>Datenbankversion </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="202"/>
        <source>Intelligent Touch Typing Tutor</source>
        <translation>Der intelligente 10-Finger-Schreibtrainer</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="203"/>
        <source>On the web: </source>
        <translation>Im Internet: </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="205"/>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation>TIPP10 wird unter den Bedingungen der GNU General Public License veröffentlicht und ist kostenlos. Sie müssen dafür nichts bezahlen, egal wo Sie die Software herunterladen!</translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="215"/>
        <source>About </source>
        <translation>Über </translation>
    </message>
    <message>
        <location filename="../widget/mainwindow.cpp" line="199"/>
        <source>Portable Version</source>
        <translation>Portable Version</translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="48"/>
        <location filename="../widget/progressionwidget.cpp" line="62"/>
        <location filename="../widget/progressionwidget.cpp" line="530"/>
        <source>Time</source>
        <translation>Zeitpunkt</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="54"/>
        <source>Show: </source>
        <translation>Zeige: </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="56"/>
        <source>All Lessons</source>
        <translation>Alle Lektionen</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="57"/>
        <source>Training Lessons</source>
        <translation>Übungslektionen</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="58"/>
        <source>Open Lessons</source>
        <translation>Freie Lektionen</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="59"/>
        <source>Own Lessons</source>
        <translation>Eigene Lektionen</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="60"/>
        <source>Order by x-axis:</source>
        <translation>X-Achse sortieren nach:</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="63"/>
        <location filename="../widget/progressionwidget.cpp" line="536"/>
        <source>Lesson</source>
        <translation>Lektion</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="241"/>
        <source>Points</source>
        <translation>Punkte</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="384"/>
        <location filename="../widget/progressionwidget.cpp" line="408"/>
        <source>Training Lesson</source>
        <translation>Übungslektion</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="391"/>
        <location filename="../widget/progressionwidget.cpp" line="415"/>
        <source>Open Lesson</source>
        <translation>Freie Lektion</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="398"/>
        <location filename="../widget/progressionwidget.cpp" line="422"/>
        <source>Own Lesson</source>
        <translation>Eigene Lektion</translation>
    </message>
    <message numerus="yes">
        <location filename="../widget/progressionwidget.cpp" line="481"/>
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n Punkt</numerusform>
            <numerusform>%n Punkte</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="482"/>
        <source>%1 cpm</source>
        <translation>%1 A/min</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="499"/>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>Der Verlauf wird erst nach der zweiten absolvierten Lektion sichtbar.</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="541"/>
        <source>Cpm</source>
        <translation>A/min</translation>
    </message>
    <message>
        <location filename="../widget/progressionwidget.cpp" line="546"/>
        <source>Score</source>
        <translation>Bewertung</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../sql/connection.h" line="116"/>
        <location filename="../sql/connection.h" line="124"/>
        <location filename="../sql/connection.h" line="144"/>
        <source>Affected directory:
</source>
        <translation>Betroffener Pfad:
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="107"/>
        <source>en</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="47"/>
        <location filename="../widget/fingerwidget.cpp" line="131"/>
        <source>Left little finger</source>
        <translation>Kleiner Finger links</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="48"/>
        <location filename="../widget/fingerwidget.cpp" line="132"/>
        <source>Left ring finger</source>
        <translation>Ringfinger links</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="49"/>
        <location filename="../widget/fingerwidget.cpp" line="133"/>
        <source>Left middle finger</source>
        <translation>Mittelfinger links</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="50"/>
        <location filename="../widget/fingerwidget.cpp" line="134"/>
        <source>Left forefinger</source>
        <translation>Zeigefinger links</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="50"/>
        <location filename="../widget/fingerwidget.cpp" line="135"/>
        <source>Right forefinger</source>
        <translation>Zeigefinger rechts</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="51"/>
        <location filename="../widget/fingerwidget.cpp" line="136"/>
        <source>Right middle finger</source>
        <translation>Mittelfinger rechts</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="52"/>
        <location filename="../widget/fingerwidget.cpp" line="137"/>
        <source>Right ring finger</source>
        <translation>Ringfinger rechts</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="53"/>
        <location filename="../widget/fingerwidget.cpp" line="138"/>
        <source>Right little finger</source>
        <translation>Kleiner Finger rechts</translation>
    </message>
    <message>
        <location filename="../sql/keyboardsql.cpp" line="53"/>
        <location filename="../widget/fingerwidget.cpp" line="138"/>
        <source>Thumb</source>
        <translation>Daumen</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <location filename="../widget/regexpdialog.ui" line="14"/>
        <source>Filter for the keyboard layout</source>
        <translation>Filter für das Tastaturlayout</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="20"/>
        <source>Limitation of characters</source>
        <translation>Zeichenbegrenzung</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="36"/>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation>Es sollen Schriftzeichen vermieden werden, die das aktuelle Tastaturlayout nicht unterstützt. Daher wird auf alle Übungstexte vor dem Training ein Filter in Form eines regulären Ausdrucks angewendet. Sie sollten Änderungen nur durchführen, wenn Sie sich mit regulären Ausdrücken auskennen.</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="46"/>
        <source>Replacement Filter</source>
        <translation>Ersetzungsfilter</translation>
    </message>
    <message>
        <location filename="../widget/regexpdialog.ui" line="61"/>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation>Durch das Filtern auf zugelassene Schriftzeichen kann sinnloser Text entstehen (z.B. durch das Entfernen von Umlauten). Sie können hier Ersetzungen definieren, die vor der Zeichenbegrenzung auf den Text angewendet werden. Verwenden Sie dazu eine Form wie in nachfolgendem Beispiel, das alle deutschen Umlaute und Eszett ersetzt:
ä=ae,ö=oe,ü=ue,Ä=Ae,Ö=Oe,Ü=Ue,ß=ss</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../widget/settings.cpp" line="78"/>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation>Einige der Einstellungen werden erst nach einem Neustart der Software wirksam.
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="295"/>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>Es werden alle absolvierten Lektionen des aktuellen Benutzers gelöscht
und die Lektionenliste in den urprünglichen Zustand versetzt!

Wollen Sie den Vorgang wirklich fortsetzen?

</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="310"/>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>Die absolvierten Lektionen wurden erfolgreich zurückgesetzt!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="321"/>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>Es werden alle aufgezeichneten Schriftzeichen (Fehlerquoten) des aktuellen Benutzers gelöscht und die Zeichenliste in den urprünglichen Zustand versetzt!

Wollen Sie den Vorgang wirklich fortsetzen?</translation>
    </message>
    <message>
        <location filename="../widget/settings.cpp" line="336"/>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>Die aufgezeichneten Schriftzeichen wurden erfolgreich zurückgesetzt!
</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="20"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="42"/>
        <source>Training</source>
        <translation>Schreibtraining</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="48"/>
        <source>Ticker</source>
        <translation>Laufschrift</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="62"/>
        <source>Here you can select the background color</source>
        <translation>Hier können Sie die Hintergrundfarbe der Laufschrift verändern</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="78"/>
        <source>Here you can select the font color</source>
        <translation>Hier können Sie die Schriftfarbe der Laufschrift verändern</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="88"/>
        <source>Cursor:</source>
        <translation>Zeiger:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="101"/>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation>Hier können Sie die Schriftart der Laufschrift verändern (eine Schriftgröße über 20 Punkte wird aus Formatierungsgründen nicht empfohlen)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="104"/>
        <source>Change &amp;Font</source>
        <translation>&amp;Schriftart ändern</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="117"/>
        <source>Here can select the color of the cursor for the current character</source>
        <translation>Hier können Sie die Farbe der Markierung für das aktuelle Zeichen verändern</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="133"/>
        <source>Font:</source>
        <translation>Schriftart:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="140"/>
        <source>Font Color:</source>
        <translation>Schriftfarbe:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="147"/>
        <source>Background:</source>
        <translation>Hintergrund:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="164"/>
        <source>Speed:</source>
        <translation>Geschwindigkeit:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="179"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="192"/>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation>Hier können Sie die Geschwindigkeit der Laufschrift verändern (Der Regler ganz links bedeutet, das Laufband bewegt sich erst am Ende der Zeile. Der Regler ganz rechts entspricht einem schnellen Laufband.)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="214"/>
        <source>Fast</source>
        <translation>Schnell</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="228"/>
        <source>Audio Output</source>
        <translation>Audioausgabe</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="234"/>
        <source>Here you can activate a metronome</source>
        <translation>Hier können Sie ein Metronom während des Schreibtrainings aktivieren</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="237"/>
        <source>Metronome:</source>
        <translation>Metronom:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="266"/>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation>Geben Sie hier an, wie oft pro Minute ein akustisches Signal ausgegeben werden soll</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="272"/>
        <source> cpm</source>
        <translation> A/min</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="292"/>
        <location filename="../widget/settings.ui" line="298"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="317"/>
        <source>Advanced</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="324"/>
        <source>Training Lessons:</source>
        <translation>Übungslektionen:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="342"/>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation>Die gewählten Übungslektionen sind nicht auf das Tastaturlayout abgestimmt. Sie können diese trotzdem trainieren, müssen aber unter Umständen von Beginn an Tastwege zurücklegen.</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="352"/>
        <source>Keyboard Layout:</source>
        <translation>Tastaturlayout:</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="376"/>
        <source>Results</source>
        <translation>Auswertung</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="382"/>
        <source>User Data</source>
        <translation>Benutzerdaten</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="388"/>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation>Hier können Sie sämtliche gespeicherte Lektionendaten zurücksetzen (die Lektionen werden so in den ursprünglichen Zustand, wie nach der Installation, versetzt)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="391"/>
        <source>Reset &amp;completed lessons</source>
        <translation>&amp;Absolvierte Lektionen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="398"/>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Hier können Sie alle aufgezeichneten Tastendrücke und Tippfehler zurücksetzen (die Schriftzeichen werden so in den ursprünglichen Zustand, wie nach der Installation, versetzt)</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="401"/>
        <source>Reset all &amp;recorded characters</source>
        <translation>&amp;Aufgezeichnete Schriftzeichen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="412"/>
        <source>Other</source>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="418"/>
        <source>Windows</source>
        <translation>Fenstereinstellungen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="424"/>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation>Hier können Sie festlegen, ob ein Informationsfenster mit Ratschlägen beim Start angezeigt werden soll</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="427"/>
        <source>Show welcome message at startup</source>
        <translation>Willkommenfenster beim Programmstart anzeigen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="434"/>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation>Hier können Sie festlegen, ob ein Hinweisfenster angezeigt werden soll, wenn eine freie oder eigene Lektion mit aktivierter Intelligenz gestartet wird</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="437"/>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Auf aktivierte Intelligenz beim Start einer freien oder
eigenen Lektion hinweisen</translation>
    </message>
    <message>
        <location filename="../widget/settings.ui" line="451"/>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Dauer der Lektion automatisch auf &quot;Gesamte Lektion&quot;
umstellen, wenn die Intelligenz deaktiviert wird</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <location filename="../widget/startwidget.cpp" line="105"/>
        <location filename="../widget/startwidget.cpp" line="753"/>
        <source>Training Lessons</source>
        <translation>Übungslektionen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="112"/>
        <source>Subject:</source>
        <translation>Thema:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="164"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="168"/>
        <source>&amp;New Lesson</source>
        <translation>&amp;Neue Lektion</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="170"/>
        <source>&amp;Import Lesson</source>
        <translation>Lektion &amp;importieren</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="172"/>
        <source>&amp;Export Lesson</source>
        <translation>Lektion &amp;exportieren</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="174"/>
        <source>&amp;Edit Lesson</source>
        <translation>Lektion &amp;editieren</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="176"/>
        <source>&amp;Delete Lesson</source>
        <translation>Lektion &amp;löschen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="205"/>
        <source>Duration of Lesson</source>
        <translation>Dauer der Lektion</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="208"/>
        <source>Time Limit:</source>
        <translation>Zeitlimit:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="210"/>
        <location filename="../widget/startwidget.cpp" line="217"/>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>Das Diktat wird nach Ablauf einer
festgelegten Zeit beendet</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="216"/>
        <source> minutes</source>
        <translation> Minuten</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="221"/>
        <source>Character Limit:</source>
        <translation>Zeichenlimit:</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="231"/>
        <source> characters</source>
        <translation> Zeichen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="223"/>
        <location filename="../widget/startwidget.cpp" line="233"/>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>Das Diktat wird nach einer bestimmten Anzahl
korrekt getippter Zeichen beendet</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="237"/>
        <source>Entire
Lesson</source>
        <translation>Gesamte
Lektion</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="239"/>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>Es wird die gesamte (freie) Lektion
von Anfang bis Ende diktiert</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="240"/>
        <source>(Entire Lesson)</source>
        <translation>(gesamte Lektion)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="270"/>
        <source>Response to Typing Errors</source>
        <translation>Reaktion auf Tippfehler</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="273"/>
        <source>Block Typing Errors</source>
        <translation>Diktat blockieren</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="275"/>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>Das Diktat wird erst fortgesetzt, wenn die
richtige Taste gedrückt wurde</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="278"/>
        <source>Correction with Backspace</source>
        <translation>Fehler korrigieren</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="280"/>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Tippfehler müssen zusätzlich über die
Rücklauftaste entfernt werden</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="283"/>
        <source>Audible Signal</source>
        <translation>Akustisches Signal</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="284"/>
        <source>A beep sounds with every typing error</source>
        <translation>Bei jedem Tippfehler ertönt ein Beepton</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="287"/>
        <source>Show key picture</source>
        <translation>Sinnbild einblenden</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="288"/>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation>Bei jedem Tippfehler wird das zur Taste entsprechende Sinnbild angezeigt</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="294"/>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Die Texte der Lektion werden nicht in ihrer vorgesehenen Reihenfolge diktiert, sondern in Echtzeit an die Tippfehler angepasst.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="300"/>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Aktivieren Sie diese Option, wenn die Texte der Lektion nicht in ihrer vorgesehenen Reihenfolge diktiert, sondern in Echtzeit an die Tippfehler angepasst werden sollen.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="307"/>
        <source>Intelligence</source>
        <translation>Intelligenz*</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="309"/>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>Anhand der aktuellen Fehlerquoten aller Schriftzeichen werdendie Worte und Sätze des Diktats in Echtzeit ausgewählt.Ist die Intelligenz dagegen deaktiviert, werden die Texte der Lektionstets in der gleichen Reihenfolge diktiert.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="341"/>
        <source>Assistance</source>
        <translation>Hilfestellungen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="343"/>
        <source>Show Keyboard</source>
        <translation>Tastatur anzeigen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="344"/>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>Zur visuellen Unterstützung werden die virtuelle
Tastatur und Statusinformationen angezeigt</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="348"/>
        <source>Colored Keys</source>
        <translation>Farbige Tasten</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="350"/>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>Zur visuellen Unterstützung werden die zu
drückenden Tasten farbig markiert</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="353"/>
        <source>Home Row</source>
        <translation>Grundstellung</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="355"/>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>Zur visuellen Unterstützung werden die
verbleibenden Finger der Grundstellung
farbig markiert</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="359"/>
        <source>L/R Separation Line</source>
        <translation>Trennlinie links/rechts</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="361"/>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>Zur visuellen Unterstützung wird eine Trennlinie
für die zu Unterscheidung von linker und rechter Hand
angezeigt</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="365"/>
        <source>Instruction</source>
        <translation>Hilfetext</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="367"/>
        <source>Show fingers to be used in the status bar</source>
        <translation>Zur Unterstützung werden die zu verwendenden
Finger in der Statusleiste angezeigt</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="370"/>
        <source>Motion Paths</source>
        <translation>Tastpfade</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="372"/>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Zur Unterstützung werden die Tastpfade der
Finger auf der Tastatur angezeigt</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="419"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="420"/>
        <source>&amp;Start Training</source>
        <translation>&amp;Schreibtraining starten</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="546"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="765"/>
        <source>Open Lessons</source>
        <translation>Freie Lektionen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="795"/>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation>Derzeit gibt es die freien Lektionen nur in deutscher Sprache. Wir arbeiten daran, bald auch freie Lektionen in englischer Sprache anbieten zu können.</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="804"/>
        <source>Own Lessons</source>
        <translation>Eigene Lektionen</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="925"/>
        <source>Please select a text file</source>
        <translation>Bitte wählen Sie eine Textdatei aus</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="926"/>
        <source>Text files (*.txt)</source>
        <translation>Textdateien (*.txt)</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1048"/>
        <source>Please indicate the location of a text file</source>
        <translation>Bitte geben Sie den Speicherort für eine Textdatei ein</translation>
    </message>
    <message>
        <location filename="../widget/startwidget.cpp" line="1097"/>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Wollen Sie die Lektion wirklich löschen und damit auch alle aufgezeichneten Daten, die im Zusammenhang mit dieser Lektion stehen?</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <location filename="../widget/tickerboard.h" line="133"/>
        <source>Press space bar to proceed</source>
        <translation>Leertaste setzt das Diktat fort</translation>
    </message>
    <message>
        <location filename="../widget/tickerboard.cpp" line="151"/>
        <source>Dictation Finished</source>
        <translation>Diktat beendet</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="129"/>
        <source>&amp;Pause</source>
        <translation>&amp;Pause</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="136"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="142"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="243"/>
        <source>Press space bar to start</source>
        <translation>Leertaste startet das Diktat</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="244"/>
        <source>Take Home Row position</source>
        <translation>Grundstellung einnehmen</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="260"/>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Wollen Sie die Lektion wirklich vorzeitig beenden?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="274"/>
        <source>Do you want to save your results?</source>
        <translation>Sollen die Ergebnisse der Lektion gespeichert werden?</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="492"/>
        <source>E&amp;xit Lesson early</source>
        <translation>Lektion vorzeitig &amp;beenden</translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="547"/>
        <source>Errors: </source>
        <translation>Fehler: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="549"/>
        <source>Cpm: </source>
        <translation>A/min: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="550"/>
        <source>Time: </source>
        <translation>Zeit: </translation>
    </message>
    <message>
        <location filename="../widget/trainingwidget.cpp" line="552"/>
        <source>Characters: </source>
        <translation>Zeichen: </translation>
    </message>
</context>
</TS>
